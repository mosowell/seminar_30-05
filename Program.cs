﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace seminar3005
{

    class MyCircle
    {
        protected double x;
        protected double y;
        protected double radius;

        public MyCircle(double x, double y, double radius)
        {
            this.x = x;
            this.y = y;
            this.radius = radius;
        }

        public bool Equals(MyCircle obj)
        {
            if (obj == null)
                return false;

            return obj.x == this.x && obj.y == this.y && obj.radius == this.radius;
        }

        public override int GetHashCode()
        {
            return (int)this.x + (int)this.y + (int)this.radius;
        }

    }
    class Program
    {
        static void Main(string[] args)
        {

            string formattedString = string.Format(new System.Globalization.CultureInfo("en-US"), "{0:dddd} Money - {1:c}", DateTime.Now, 15);

            DateTime [] dates = new DateTime[4];
            double[] temperature = new double[4] { 26.3, 27.1, 30, 24.7 };

            dates[0] = new DateTime();
            dates[1] = new DateTime(2015, 7, 20, 18, 30, 5);
            dates[2] = new DateTime(2013, 3, 10, 12, 10, 20);
            dates[3] = new DateTime(2018, 7, 30, 8, 30, 55);

            if (dates.Length !=temperature.Length)
            {
                Console.WriteLine("Несоответствие длин!");
                Environment.Exit(-1);
            }

            for (int i = 0; i < dates.Length; i++)
            {
                string res = String.Format("{0:MMM dd (ddd) H:mm} > {1:f1}",
                    dates[i],
                    temperature[i]);

                
                Console.WriteLine(Char.ToUpper(res[0]) + res.Substring(1));
            }
        }
    }
}
